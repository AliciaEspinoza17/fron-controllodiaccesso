import { Component,OnInit } from '@angular/core';
import { ServicioControlloService } from 'src/app/service/servicio-controllo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css']

})
export class AlumnoComponent {
  datos: any[] = [];

  constructor (private _service:ServicioControlloService){}
  ngOnInit(): void {
    this.consulta();
  }
  
  borrarProducto(id:any){

    Swal.fire({
      title: '¡ADVERTENCIA!',
      text: '¿Desea eliminar de forma permanente el producto?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#31f700',
      cancelButtonColor: '#f70000',
      confirmButtonText: 'ACEPTAR',
      cancelButtonText: 'CANCELAR'
    })
  }

  guardarId(id : any){
    const objectId = id;
    localStorage.setItem('objectId', objectId);
    console.log("Id del usuario: ",objectId);
  }
  consulta(){
    this._service.getDatosAlu().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos=resp.respuesta;
    });
  }
}
