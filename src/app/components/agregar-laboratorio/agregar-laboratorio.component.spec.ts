import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarLaboratorioComponent } from './agregar-laboratorio.component';

describe('AgregarLaboratorioComponent', () => {
  let component: AgregarLaboratorioComponent;
  let fixture: ComponentFixture<AgregarLaboratorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarLaboratorioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgregarLaboratorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
