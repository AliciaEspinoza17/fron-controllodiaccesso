import { Component,OnInit } from '@angular/core';
import { ServicioControlloService } from 'src/app/service/servicio-controllo.service';
@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.css']
})
export class DocenteComponent {
  datos: any[] = [];
  constructor (private _service:ServicioControlloService){}
  ngOnInit(): void {
    this.consulta();
  }

  consulta(){
    this._service.getDatosDoc().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos=resp.respuesta;
    });
  }
}
