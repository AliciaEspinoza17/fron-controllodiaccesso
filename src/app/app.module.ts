import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { AlumnoComponent } from './components/alumno/alumno.component';
import { AgregarAlumnoComponent } from './components/agregar-alumno/agregar-alumno.component';
import { AgregarDocenteComponent } from './components/agregar-docente/agregar-docente.component';
import { AgregarLaboratorioComponent } from './components/agregar-laboratorio/agregar-laboratorio.component';
import { EditarAlumnoComponent } from './components/editar-alumno/editar-alumno.component';
import { EditarDocenteComponent } from './components/editar-docente/editar-docente.component';
import { EditarLaboratorioComponent } from './components/editar-laboratorio/editar-laboratorio.component';
import { DocenteComponent } from './components/docente/docente.component';
import { LaboratorioComponent } from './components/laboratorio/laboratorio.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    AlumnoComponent,
    AgregarAlumnoComponent,
    AgregarDocenteComponent,
    AgregarLaboratorioComponent,
    EditarAlumnoComponent,
    EditarDocenteComponent,
    EditarLaboratorioComponent,
    DocenteComponent,
    LaboratorioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
