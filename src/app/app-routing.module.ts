import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlumnoComponent } from './components/alumno/alumno.component';
import { DocenteComponent } from './components/docente/docente.component';
import { LaboratorioComponent } from './components/laboratorio/laboratorio.component';
import { AgregarAlumnoComponent } from './components/agregar-alumno/agregar-alumno.component';
import { AgregarDocenteComponent } from './components/agregar-docente/agregar-docente.component';
import { AgregarLaboratorioComponent } from './components/agregar-laboratorio/agregar-laboratorio.component';
import { EditarAlumnoComponent } from './components/editar-alumno/editar-alumno.component';
import { EditarDocenteComponent } from './components/editar-docente/editar-docente.component';
import { EditarLaboratorioComponent } from './components/editar-laboratorio/editar-laboratorio.component';


const routes: Routes = [

    {
        path : 'Inicio/Alumno',
        component : AlumnoComponent
    },
    {
        path : 'Inicio/Docente',
        component : DocenteComponent
    },
    {
        path : 'Inicio/Laboratorio',
        component : LaboratorioComponent
    },
    {
        path : 'Inicio/AgregarAlumno',
        component : AgregarAlumnoComponent
    },
    {
        path : 'Inicio/AgregarDocente',
        component : AgregarDocenteComponent
    },
    {
        path : 'Inicio/AgregarLaboratorio',
        component : AgregarLaboratorioComponent
    },
    {
        path : 'Inicio/EditarAlumno',
        component : EditarAlumnoComponent
    },
    {
        path : 'Inicio/EditarDocente',
        component : EditarDocenteComponent
    },
    {
        path : 'Inicio/EditarLaboratorio',
        component : EditarLaboratorioComponent
    },
    {
        path : '',
        redirectTo : 'Inicio/Alumno',
        pathMatch : 'full',

    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }