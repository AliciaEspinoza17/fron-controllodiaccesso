import { TestBed } from '@angular/core/testing';

import { ServicioControlloService } from './servicio-controllo.service';

describe('ServicioControlloService', () => {
  let service: ServicioControlloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicioControlloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
