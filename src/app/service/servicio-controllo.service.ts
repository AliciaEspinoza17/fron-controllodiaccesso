import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicioControlloService {

  public URL='http://127.0.0.1:3900'
  public url:any;
  constructor(private _http:HttpClient) { 
    this.url=this.URL;
  }
  public getDatosAlu(){
    const url=`${this.url}/obtener/alumno`
    return this._http.get(url);
  }
  public getDatosDoc(){
    const url=`${this.url}/obtener/docente`
    return this._http.get(url);
  }
}